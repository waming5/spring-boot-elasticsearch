package com.waming.spring.boot.elasticsearch.mapper;

import com.waming.spring.boot.elasticsearch.entity.Company;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CompanyRepository extends ElasticsearchRepository<Company,Long> {
}