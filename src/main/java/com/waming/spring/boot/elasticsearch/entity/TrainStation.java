package com.waming.spring.boot.elasticsearch.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
/**
 * 车站
 */
public class TrainStation implements Serializable {
    private static final long serialVersionUID = -2583594456405352660L;
    /**
     * id
     */
    @Id
    private String id;
    /**
     * 车站三字码
     */
    @Field(type = FieldType.Keyword)
    private String code;

    /**
     * 车站名称
     */
    @Field(type = FieldType.Text,analyzer = "ik_smart")
    private String name;

    /**
     * 车站拼音
     */
    @Field(type = FieldType.Text)
    private String pinyin;

    /**
     * 车站简拼
     */
    @Field(type = FieldType.Text)
    @JsonProperty("simply_pinyin")
    private String simplyPinyin;

    /**
     * 基础城市ID
     */
    @Field(type = FieldType.Keyword)
    @JsonProperty("city_code")
    private String cityCode;
    /**
     * 城市名称
     */
    @Field(type = FieldType.Text,analyzer = "ik_smart")
    @JsonProperty("city_name")
    private String cityName;
    /**
     * 城市拼音
     */
    @Field(type = FieldType.Text)
    @JsonProperty("city_pinyin")
    private String cityPinyin;

    /**
     * 城市简拼
     */
    @Field(type = FieldType.Text)
    @JsonProperty("city_simply_pinyin")
    private String citySimplyPinyin;

    /**
     * 父级城市代码，最高只定位到市一级
     */
    @Field(type = FieldType.Keyword)
    @JsonProperty("parent_city_code")
    private String parentCityCode;

    /**
     * 省名
     */
    @Field(type = FieldType.Keyword)
    @JsonProperty("privince_name")
    private String privinceName;
    /**
     * 国家码
     */
    @Field(type = FieldType.Keyword)
    @JsonProperty("country_code")
    private String countryCode;

    /**
     * 热门度
     */
    @Field(type = FieldType.Integer)
    private Integer hot;

    /**
     * 状态
     */
    @Field(type = FieldType.Integer)
    private Integer status;

    /**
     * 创建时间
     */
    @Field(type = FieldType.Long)
    @JsonProperty("created_at")
    private Long createdAt;

    /**
     * 修改时间
     */
    @Field(type = FieldType.Long)
    @JsonProperty("updated_at")
    private Long updatedAt;

    /**
     * 是否是默认车站
     */
    @Field(type = FieldType.Integer)
    @JsonProperty("default_station")
    private Integer defaultStation;

    /**
     * 大唐拼音
     */
    @Field(type = FieldType.Keyword)
    @JsonProperty("dt_pinyin")
    private String dtPinyin;
    /**
     * 思客拼音
     */
    @Field(type = FieldType.Keyword)
    @JsonProperty("ceekee_pinyin")
    private String ceekeePinyin;

    public String getId() {
        return id;
    }
    public TrainStation setId(String id) {
        this.id = id;
        return this;
    }

    public String getCode() {
        return code;
    }
    public TrainStation setCode(String code) {
        this.code = code;
        return this;
    }

    public String getName() {
        return name;
    }
    public TrainStation setName(String name) {
        this.name = name;
        return this;
    }

    public String getPinyin() {
        return pinyin;
    }
    public TrainStation setPinyin(String pinyin) {
        this.pinyin = pinyin;
        return this;
    }

    public String getSimplyPinyin() {
        return simplyPinyin;
    }
    public TrainStation setSimplyPinyin(String simplyPinyin) {
        this.simplyPinyin = simplyPinyin;
        return this;
    }

    public String getCityCode() {
        return cityCode;
    }
    public TrainStation setCityCode(String cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    public String getCityName() {
        return cityName;
    }
    public TrainStation setCityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public String getCityPinyin() {
        return cityPinyin;
    }
    public TrainStation setCityPinyin(String cityPinyin) {
        this.cityPinyin = cityPinyin;
        return this;
    }

    public String getCitySimplyPinyin() {
        return citySimplyPinyin;
    }
    public TrainStation setCitySimplyPinyin(String citySimplyPinyin) {
        this.citySimplyPinyin = citySimplyPinyin;
        return this;
    }

    public String getParentCityCode() {
        return parentCityCode;
    }
    public TrainStation setParentCityCode(String parentCityCode) {
        this.parentCityCode = parentCityCode;
        return this;
    }

    public String getPrivinceName() {
        return privinceName;
    }
    public TrainStation setPrivinceName(String privinceName) {
        this.privinceName = privinceName;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }
    public TrainStation setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public Integer getHot() {
        return hot;
    }
    public TrainStation setHot(Integer hot) {
        this.hot = hot;
        return this;
    }

    public Integer getStatus() {
        return status;
    }
    public TrainStation setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Long getCreatedAt() {
        return createdAt;
    }
    public TrainStation setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }
    public TrainStation setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Integer getDefaultStation() {
        return defaultStation;
    }
    public TrainStation setDefaultStation(Integer defaultStation) {
        this.defaultStation = defaultStation;
        return this;
    }

    public String getDtPinyin() {
        return dtPinyin;
    }
    public TrainStation setDtPinyin(String dtPinyin) {
        this.dtPinyin = dtPinyin;
        return this;
    }

    public String getCeekeePinyin() {
        return ceekeePinyin;
    }
    public TrainStation setCeekeePinyin(String ceekeePinyin) {
        this.ceekeePinyin = ceekeePinyin;
        return this;
    }

    @Override
    public String toString() {
        return "TrainStation{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", simplyPinyin='" + simplyPinyin + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", cityName='" + cityName + '\'' +
                ", cityPinyin='" + cityPinyin + '\'' +
                ", citySimplyPinyin='" + citySimplyPinyin + '\'' +
                ", parentCityCode='" + parentCityCode + '\'' +
                ", privinceName='" + privinceName + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", hot=" + hot +
                ", status=" + status +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", defaultStation=" + defaultStation +
                ", dtPinyin='" + dtPinyin + '\'' +
                ", ceekeePinyin='" + ceekeePinyin + '\'' +
                '}';
    }
}
