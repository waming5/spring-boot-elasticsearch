package com.waming.spring.boot.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * 企业
 */
@Document(indexName = "company_index",type ="company",shards =3,replicas =0)
public class Company implements java.io.Serializable{
    private static final long serialVersionUID = 7471255210930359679L;
    @Id
    private Long id;

    @Field(type = FieldType.Keyword)
    private String name;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String address;

    @Field(type = FieldType.Keyword)
    private String mobile;

    @Field(type = FieldType.Keyword)
    private String email;

    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String nature;

    @Field(type = FieldType.Text)
    private String website;

    public Long getId() {
        return id;
    }
    public Company setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }
    public Company setName(String name) {
        this.name = name;
        return this;
    }

    public String getAddress() {
        return address;
    }
    public Company setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getMobile() {
        return mobile;
    }
    public Company setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getEmail() {
        return email;
    }
    public Company setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getNature() {
        return nature;
    }
    public Company setNature(String nature) {
        this.nature = nature;
        return this;
    }

    public String getWebsite() {
        return website;
    }
    public Company setWebsite(String website) {
        this.website = website;
        return this;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", nature='" + nature + '\'' +
                ", website='" + website + '\'' +
                '}';
    }
}
