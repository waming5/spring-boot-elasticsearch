package com.waming.spring.boot.elasticsearch.mapper;

import com.waming.spring.boot.elasticsearch.entity.User;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import java.util.List;

public interface UserRepository extends ElasticsearchRepository<User,Long> {
    /**
     * 根据名字查询
     * @param name
     * @return
     */
    List<User> findByName(String name);

    @Query("{\"range\":{\"id\":{\"from\":\"?0\",\"to\":\"?1\"}}}")
    List<User> queryByIdSql(Long start, Long end);

    @Query("{\"match\": {\"name\": {\"query\": \"?0\"}}}")
    List<User> queryByNameSql(String name);
}
