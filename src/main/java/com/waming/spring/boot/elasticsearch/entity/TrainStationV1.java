package com.waming.spring.boot.elasticsearch.entity;
import org.springframework.data.elasticsearch.annotations.Document;
/**
 * 车站V1版本
 */
@Document(indexName = "train_station_v1_index",replicas = 1 ,shards =3)
public class TrainStationV1 extends TrainStation{
    private static final long serialVersionUID = -2113026752141540271L;
}
