package com.waming.spring.boot.elasticsearch.service;

import com.waming.spring.boot.elasticsearch.entity.TrainStation;
import java.util.List;
import java.util.Map;

public interface ITrainStationService {

    /**
     * 添加别名
     *
     * @param aliasName
     * @param indexName
     * @return
     */
    boolean createAlias(String aliasName, String indexName);

    /**
     * 删除别名
     *
     * @param aliasName
     * @param indexName
     * @return
     */
    boolean deleteAlias(String aliasName, String indexName);


    /**
     * 根据关键字搜索 （中文名称、英文名称、拼音、城市名称）
     * @param keyword
     * @param indexName
     * @return
     */
    List<TrainStation> searchByKeyword(String keyword,String indexName);

    /**
     * @param keyword
     * @param indexName 索引名
     * @return
     */
    List<TrainStation> searchUsersByKeywordWithAlias(String keyword,String indexName);

    /**
     * 根据城市代码查询默认站
     * @param cityCodes 城市代码
     * @return
     */
    Map<String, TrainStation> searchDefault(List<String> cityCodes,String indexName);

    /**
     * 批量保存
     * @param trainStations
     * @return
     */
    Boolean batchSave(List<TrainStation> trainStations);

    /**
     * 更新
     * @param trainStation
     * @return
     */
    Boolean update(TrainStation trainStation);

    /**
     * 删除
     * @param trainStation
     * @return
     */
    Boolean delete(TrainStation trainStation);
}
