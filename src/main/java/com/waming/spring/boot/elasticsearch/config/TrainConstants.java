package com.waming.spring.boot.elasticsearch.config;

public interface TrainConstants {
    String TRAIN_STATION_V1 = "train_station_v1_index";

    String TRAIN_STATION_V2 = "train_station_v2_index";

    String TRAIN_STATION_ALIASNAME= "train_station";
}
