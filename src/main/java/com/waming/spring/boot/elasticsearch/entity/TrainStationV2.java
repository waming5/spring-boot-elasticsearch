package com.waming.spring.boot.elasticsearch.entity;

import org.springframework.data.elasticsearch.annotations.Document;

/**
 * 车站V2版本
 */
@Document(indexName = "train_station_v2_index",replicas = 1 ,shards =3)
public class TrainStationV2 extends TrainStation{
    private static final long serialVersionUID = -2400197756281360963L;
}
