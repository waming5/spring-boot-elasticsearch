package com.waming.spring.boot.elasticsearch.mapper;

import com.waming.spring.boot.elasticsearch.entity.TrainStationV2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
/**
 *
 */
@Repository
public interface TrainStationV2Repository extends ElasticsearchRepository<TrainStationV2, String> {
    @Query("{\"bool\":{\"must\":[{\"function_score\":{\"query\":{\"multi_match\":{\"query\":\"?0\",\"fields\":[\"name^2\",\"pinyin^2\",\"cityName^1\",\"cityPinyin^1\"]}},\"score_mode\":\"sum\"}}],\"filter\":[{\"term\":{\"status\":\"1\"}}]}}")
    Page<TrainStationV2> searchByNameAndPinyinAndNameEn(String keyword, Pageable pageable);
}
