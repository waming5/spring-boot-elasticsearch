package com.waming.spring.boot.elasticsearch.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * Elasticsearch配置类
 */
@Configuration
@EnableElasticsearchRepositories(basePackages = "com.waming.spring.boot.elasticsearch.mapper")
public class ElasticsearchConfig {
    // 配置相关的Elasticsearch连接和其他属性
}
