package com.waming.spring.boot.elasticsearch;

import com.waming.spring.boot.elasticsearch.entity.Company;
import com.waming.spring.boot.elasticsearch.entity.User;
import com.waming.spring.boot.elasticsearch.mapper.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class UserRepositoryTest {
    private final Logger log = LogManager.getLogger(UserRepositoryTest.class);
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    @Test
    public void saveTest() {
        Company c = new Company();
        c.setName("百度在线网络技术（北京）有限公司");
        c.setAddress("北京市海淀区中关村街道");
        c.setMobile("18510367878");
        c.setEmail("123@baidu.com");
        c.setNature("互联网");
        c.setWebsite("https://www.baidu.com/");
        List<User> users = new ArrayList<>();
        for (long i = 0; i < 1000000; i++) {
            User p = new User();
            p.setId(i);
            p.setName("李四"+i);
            p.setAddress("北京市海淀区中关村街道");
            p.setMobile("16518323"+i);
            p.setEmail("lisi"+i+"@baidu.com");
            p.setBirthday(LocalDate.of(1999, 2, 12));
            p.setCompany(c);
            users.add(p);
            if (users.size()>=1000){
                userRepository.saveAll(users);
                users = new ArrayList<>();
            }
        }
        if (!users.isEmpty()){
            userRepository.saveAll(users);
        }
    }


    @Test
    public void findByName(){
        List<User> users=userRepository.findByName("李四41023");
        log.info("users:{}",users);
    }

    @Test
    public void queryByIdSql(){
        List<User> users=userRepository.queryByIdSql(1L,100L);
        for (User user:users){
            System.out.println(user);
        }
    }

    @Test
    public void search(){
        Query query=new NativeSearchQuery(QueryBuilders.matchQuery("name","李四329").operator(Operator.AND));
        query.setPageable(PageRequest.of(0,10));
        SearchHits<User> hits=elasticsearchOperations.search(query,User.class, IndexCoordinates.of("user_index"));
        log.info("hits:{}",hits);
        for (SearchHit<User> hit:hits.getSearchHits()){
            System.out.println(hit.getContent());
        }
    }

}
