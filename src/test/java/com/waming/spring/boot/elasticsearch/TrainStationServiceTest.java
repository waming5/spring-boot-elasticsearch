package com.waming.spring.boot.elasticsearch;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.waming.spring.boot.elasticsearch.config.TrainConstants;
import com.waming.spring.boot.elasticsearch.entity.TrainStation;
import com.waming.spring.boot.elasticsearch.entity.TrainStationV1;
import com.waming.spring.boot.elasticsearch.entity.TrainStationV2;
import com.waming.spring.boot.elasticsearch.service.ITrainStationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StringUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@SpringBootTest
public class TrainStationServiceTest {
    @Autowired
    private ITrainStationService trainStationService;

    @Test
    public void save1() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new ClassPathResource("basics_train_station_export_2023-08-09_180013.json").getFile();
        TypeReference<List<TrainStationV1>> typeReference = new TypeReference<List<TrainStationV1>>() {};
        List<TrainStationV1> stations = objectMapper.readValue(file, typeReference);
        List<TrainStation> trainStations = new ArrayList<>();
        for (TrainStationV1 station : stations) {
            if(!StringUtils.hasText(station.getCityCode())){
                System.err.println(station.getCityCode()+"  "+station.getName());
            }
            trainStations.add(station);
            if (trainStations.size() >= 500) {
                trainStationService.batchSave(trainStations);
                trainStations = new ArrayList<>();
            }
        }
        if (!trainStations.isEmpty()) {
            trainStationService.batchSave(trainStations);
        }
    }

    @Test
    public void save2() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new ClassPathResource("basics_train_station_export_2023-08-09_180013.json").getFile();
        TypeReference<List<TrainStationV2>> typeReference = new TypeReference<List<TrainStationV2>>() {};
        List<TrainStationV2> stations = objectMapper.readValue(file, typeReference);
        List<TrainStation> trainStations = new ArrayList<>();
        for (TrainStationV2 station : stations) {
            if(!StringUtils.hasText(station.getCityCode())){
                System.err.println(station.getCityCode()+"  "+station.getName());
            }
            trainStations.add(station);
            if (trainStations.size() >= 500) {
                trainStationService.batchSave(trainStations);
                trainStations = new ArrayList<>();
            }
        }
        if (!trainStations.isEmpty()) {
            trainStationService.batchSave(trainStations);
        }
    }

    @Test
    public void add() {
        Boolean rs=trainStationService.createAlias(TrainConstants.TRAIN_STATION_ALIASNAME,TrainConstants.TRAIN_STATION_V1);
        System.out.println(rs);
    }

    @Test
    public void remove() {
        Boolean rs=trainStationService.deleteAlias(TrainConstants.TRAIN_STATION_ALIASNAME,TrainConstants.TRAIN_STATION_V1);
        System.out.println(rs);
    }


    @Test
    public void search() {
        List<TrainStation> trainStations=trainStationService.searchUsersByKeywordWithAlias("北京", TrainConstants.TRAIN_STATION_V1);
        System.out.println(trainStations.size());
        for (TrainStation trainStation : trainStations) {
            System.out.println(trainStation);
        }
    }

    @Test
    public void searchKey() {
        List<TrainStation> trainStations=trainStationService.searchByKeyword("安康",TrainConstants.TRAIN_STATION_ALIASNAME);
        System.out.println(trainStations.size());
        for (TrainStation trainStation : trainStations) {
            if(!StringUtils.hasText(trainStation.getCityCode())){
                System.err.println(trainStation);
            }else {
                System.out.println(trainStation);
            }
        }
    }

    @Test
    public void searchDefault() {
        List<String> trains=new ArrayList<>(4);
        trains.add("211300");
        trains.add("510131");
        trains.add("110100");
        trains.add("130800");
        Map<String,TrainStation> trainStations=trainStationService.searchDefault(trains,TrainConstants.TRAIN_STATION_ALIASNAME);
        System.out.println(trainStations.size());
        for (Map.Entry<String,TrainStation> entry : trainStations.entrySet()) {
            System.err.println(entry.getValue());
        }
    }
}
